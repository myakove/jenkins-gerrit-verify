#! /usr/bin/python
# -*- coding: utf-8 -*-

"""
Run Jenkins jobs with parameters and set Gerrit review on patches (+1, 0, -1)
based on the run results
"""

import logging
import os
import re
import sys
from configparser import ConfigParser
from difflib import SequenceMatcher
from time import sleep

import click
import jenkins as jenkins_api
import progressbar
from colorlog import ColoredFormatter
from gerrit import reviews

logging.captureWarnings(True)

C = {
    "green": "\033[0;32m",
    "red": "\033[0;31m",
    "yellow": "\033[1;33m",
    "clear": "\033[0m",
}
ACTION_LIST = [
    "RUN_GE",
    "CLEAN_GE",
    "REPRO_RESOURCES",
    "INSTALL_RHV",
    "BUILD_GE",
    "UPDATE_GE",
    "UPGRADE_GE",
    "BUILD_GE_USING_ANSIBLE",
    "CODE_COVERAGE",
    "CLEAN_GE_USING_ANSIBLE",
]
STORAGE_YAML_DEFAULT = "nfs"
STORAGE_TYPE_DEFAULT = "iscsi"
API_DEFAULT = "rest"
HOST_ORDER_DEFAULT = "dont_change"
GERRIT_CONF_DEFAULT = os.path.join(os.path.expanduser('~'), ".gerrit.config")
JENKINS_CONF_DEFAULT = os.path.join(os.path.expanduser('~'), ".jenkins.config")
PRODUCT_DEFAULT = "rhevm"
ACTION_DEFAULT = [ACTION_LIST[0]]
FAILED = "ERROR"
PASSED = "PASSED"
SKIPPED = "SKIPPED"


def prepare_logger():
    """
    Prepare logger object with colors

    Returns:
        logging.getLogger: Logger object
    """
    log_formatter = ColoredFormatter(
        "%(asctime)s %(log_color)s%(levelname)" "-8s%(reset)s%(white)s%(message)s",
        log_colors={
            "DEBUG": "cyan",
            "INFO": "green",
            "WARNING": "yellow",
            "ERROR": "red",
            "CRITICAL": "red,bg_white",
        },
        secondary_log_colors={},
    )
    logger_obj = logging.getLogger(__name__)
    log_handler = logging.StreamHandler()
    log_handler.setFormatter(log_formatter)
    logger_obj.setLevel(logging.INFO)
    logger_obj.addHandler(log_handler)
    return logger_obj


logger = prepare_logger()


def parse_config(conf_file):
    parser = ConfigParser()
    # Open the file with the correct encoding
    parser.read(conf_file, encoding="utf-8")
    params_dict = {}
    for params in parser.items("SETTING"):
        params_dict[params[0]] = params[1]

    return params_dict


def get_jenkins_connection(conf_file):
    """
    Get Jenkins connection object
    Args:
        conf_file (str): Config file path with jenkins params

    Returns:
        Jenkins: Jenkins connection
    """
    os.environ["PYTHONHTTPSVERIFY"] = "0"
    params = parse_config(conf_file=conf_file)
    server = params.get("server")
    username = params.get("username")
    password = params.get("password")

    return jenkins_api.Jenkins(url=server, username=username, password=password)


def gerrit_get_connection_params(conf_file):
    """
    Get gerrit connection params from conf file

    Args:
        conf_file (str): Gerrit config file

    Returns:
        tuple: host, username, rsa_key
    """
    params = parse_config(conf_file=conf_file)
    host = params["server"]
    username = params["username"]
    rsa_key = params.get("rsa_key", f"{os.path.expanduser('~')}/.ssh/id_rsa")
    return host, username, rsa_key


def gerrit_get_patch_subject(conf_file, patch):
    """
    Get gerrit patch subject

    Args:
        conf_file (str):  Gerrit config file
        patch (str): Gerrit patch number

    Returns:
         str: Gerrit patch subject
    """
    host, username, rsa_key = gerrit_get_connection_params(conf_file=conf_file)
    query = reviews.Query(host, user=username, key=rsa_key)
    for p_ in query.filter(patch):
        return p_.get("subject")


def gerrit_verify(conf_file, patch, status, msg):
    """
    Set verify score to gerrit patch

    Args:
        conf_file (str): Gerrit config file
        patch (str): Patch set (refs/changes/83/62283/7)
        status (int): Status to set (1, 0, -1)
        msg (str): Commit message
    """
    if status == 0:
        logger.info("Not verifying {patch}".format(patch=patch))
        return

    host, username, rsa_key = gerrit_get_connection_params(conf_file=conf_file)
    _p_to_rev = patch.rsplit("/")[-2:]
    p_to_rev = ",".join(_p_to_rev)
    rev = reviews.Review(review=p_to_rev, host=host, user=username, key=rsa_key)

    status_for_log = "+1" if status == 1 else "-1"
    logger.info(
        "Setting {patch} to {status_for_log} with {msg}".format(
            patch=patch, status_for_log=status_for_log, msg=msg
        )
    )
    rev.verify(status)
    rev.commit(msg)


def get_build_params(
    slave_lable,
    refs,
    pytest_params,
    cluster_domain,
    cluster_name,
    bare_metal,
    region,
    report_portal,
    **params
):
    params["SLAVE_LABEL"] = slave_lable
    if refs:
        params["REFS"] = refs

    params["PYTEST_PARAMS"] = "-o log_cli=true"
    if pytest_params:
        params["PYTEST_PARAMS"] += " {pytest_params}".format(
            pytest_params=pytest_params
        )

    params["CLUSTER_DOMAIN"] = cluster_domain or "oc4"
    params["CLUSTER_NAME"] = cluster_name or "working"
    params["PURE_BAREMETAL"] = bare_metal
    params["RP_ENABLED"] = report_portal
    params["REGION"] = region

    return params


def get_params_to_logger_and_jenkins(repo_dict, gerrit_conf_file, **params):
    """
    Get params for Jenkins build() and for logger

    Args:
        repo_dict (dict): Git repositories dict
        gerrit_conf_file (str): Gerrit config file path

    Returns:
        tuple: Params for logger and params for Jenkins

    """
    params_to_logger = dict()
    params_to_jenkins = dict()
    for k, v in params.items():
        patches = list()
        if not v:
            continue

        params_to_jenkins[k] = v
        if k in repo_dict.keys():
            for r_patch in v.split():
                patch = r_patch.rsplit("/")[-2]
                p_subject = gerrit_get_patch_subject(
                    conf_file=gerrit_conf_file, patch=patch
                )
                p_ = "{val} {p_subject}".format(val=r_patch, p_subject=p_subject)
                patches.append(p_)
        params_to_logger[k] = patches or v

    return params_to_logger, params_to_jenkins


def guess_name(names_list, name):
    """
    Try to guess name name if name not found in name list

    Args:
        names_list (list): List of all jobs
        name (str): Job name

    Returns:
        str: Available name if guessed
    """
    match = ""
    score = 0
    for j in names_list:
        j_score = SequenceMatcher(a=name, b=j).ratio()
        if j_score > score:
            score = j_score
            match = j
    return match


def get_all_jobs():
    jenkins_connection = get_jenkins_connection(conf_file=JENKINS_CONF_DEFAULT)
    return jenkins_connection.get_all_jobs()


def get_all_jobs_names():
    return [i.get("name") for i in get_all_jobs()]


def build_job_and_verify(
    job,
    slave,
    gerrit_conf_file=GERRIT_CONF_DEFAULT,
    jenkins_conf_file=JENKINS_CONF_DEFAULT,
    pytest_params=None,
    refs=None,
    cluster_domain=None,
    cluster_name=None,
    view=None,
    progress=True,
    bare_metal=False,
    region="USA",
    report_portal=False,
):
    """
    Run jenkins job and verify patch if any on gerrit.
    """
    output = ""
    failed_tests = []

    repo_dict = {"REFS": "cnv-tests"}
    jenkins_connection = get_jenkins_connection(conf_file=jenkins_conf_file)
    all_jobs_names = get_all_jobs_names()
    if job not in all_jobs_names:
        match = guess_name(names_list=all_jobs_names, name=job)
        error_1 = "Job {job} not found".format(job=job)
        error_2 = "Did you mean: {env}".format(env=match)
        if progress:
            click.secho(error_1, fg="red")
            click.secho(error_2, fg="green")
        else:
            output += "{out}\n".format(out=error_1)
            output += "{out}\n".format(out=error_2)
        sys.exit(1)

    params = dict()

    current_build_number = 0
    if not view:
        params = get_build_params(
            slave_lable=slave,
            refs=refs,
            pytest_params=pytest_params,
            cluster_domain=cluster_domain,
            cluster_name=cluster_name,
            bare_metal=bare_metal,
            region=region,
            report_portal=report_portal,
            **params
        )
        params_to_logger, params_to_jenkins = get_params_to_logger_and_jenkins(
            repo_dict=repo_dict, gerrit_conf_file=gerrit_conf_file, **params
        )

        for_log = "Running {job} with:".format(job=job)
        if progress:
            logger.info(for_log)
            for k, v in params_to_logger.items():
                if isinstance(v, list):
                    idx = 0
                    for i in v:
                        if idx != 0:
                            key = " " * len(k)
                            logger.info("{key}  {value}".format(key=key, value=i))
                        else:
                            logger.info("{key}: {value}".format(key=k, value=i))
                        idx = +1
                else:
                    logger.info("{key}: {value}".format(key=k, value=v))
        else:
            output += "{out}\n".format(out=for_log)

        job_info = jenkins_connection.get_job_info(job)
        try:
            last_build = job_info.get("builds")[0]
            last_build_number = last_build.get("number")
        except IndexError:
            last_build_number = 0

        jenkins_connection.build_job(name=job, parameters=params_to_jenkins)
        current_build = dict()
        found_running_job = False
        while not found_running_job:
            job_info = jenkins_connection.get_job_info(job)
            current_build = job_info.get("builds")[0]
            current_build_number = current_build.get("number")
            if current_build_number > last_build_number:
                current_job = jenkins_connection.get_build_info(
                    name=job, number=current_build_number
                )
                current_job_params = current_job.get("actions")[0].get("parameters")
                should_break = False
                for k, v in params_to_jenkins.items():
                    for param in current_job_params:
                        if param.get("name") == k:
                            if param.get("value") != v:
                                should_break = True
                                break
                    if should_break:
                        break

                found_running_job = not should_break
            sleep(1)

        build_url = current_build.get("url")
        if progress:
            for_log = "Build started: {0}".format(build_url)
            logger.info(for_log)
        else:
            output += "{out}\n".format(out=for_log)

    current_build_number = view if view else current_build_number
    build_params = jenkins_connection.get_build_info(
        name=job, number=current_build_number
    )

    job_url = build_params.get("url")
    status = build_params.get("building")
    init_test_to_log = ""
    format_custom_text = progressbar.FormatCustomText("%(line)s", dict(line=""))
    pbar_widgets = [progressbar.Timer(), format_custom_text]
    pbar = progressbar.ProgressBar(
        max_value=progressbar.UnknownLength, widgets=pbar_widgets, redirect_stdout=True
    )
    if progress:
        idx = 0
        percent = "[  0%]"
        progress = percent
        while status:
            pbar.update()
            sleep(1)
            if not idx % 10:
                build_params = jenkins_connection.get_build_info(
                    name=job, number=current_build_number
                )
                status = build_params.get("building")
                job_console = jenkins_connection.get_build_console_output(
                    name=job, number=current_build_number
                )
                tests = re.findall(r"::\w+", job_console)
                set_tests = set(tests)
                tests = sorted(set_tests)
                passed = len(re.findall(PASSED, job_console))
                skipped = len(re.findall(SKIPPED, job_console))
                failed = len(re.findall(FAILED, job_console))
                curr_test = tests[-1] if tests else None
                if curr_test and curr_test != init_test_to_log:
                    new_percent = re.findall(r"\[.*\d+%\]", job_console)
                    if new_percent:
                        new_percent = new_percent[-1]
                        if new_percent != percent:
                            percent = new_percent
                            progress = new_percent

                    init_test_to_log = curr_test
                    test_name = curr_test.strip("::")
                    line = " ### {test_num} {test_name} ###".format(
                        test_num=progress, test_name=test_name
                    )
                    if passed:
                        line += " [{color}{num}{clear}] ".format(
                            color=C["green"], num=passed, clear=C["clear"]
                        )

                    if skipped:
                        line += " [{color}{num}{clear}] ".format(
                            color=C["yellow"], num=skipped, clear=C["clear"]
                        )

                    if failed:
                        line += " [{color}{num}{clear}] ".format(
                            color=C["red"], num=failed, clear=C["clear"]
                        )
                        failed_tests.append(line)

                    format_custom_text.update_mapping(line=line)

            idx += 1

        print("\n")

    res = build_params.get("result")
    for_log = "Build {job} finished: {res}".format(job=job, res=res)
    if progress:
        logger.info(for_log)
        if failed_tests:
            logger.info("The following tests failed:\n")
            for failed_test in failed_tests:
                logger.info("\t{failed_test}".format(failed_test=failed_test))
    else:
        output += "{out}\n".format(out=for_log)

    if not progress:
        print(output)

    _build_param_ = [i for i in build_params.get("actions") if i.get("parameters")]
    if not _build_param_:
        sys.exit()

    all_patches = [
        i.get("value").split(" ")
        for i in _build_param_[0].get("parameters")
        if i.get("name") in repo_dict.keys() and i.get("value")
    ]
    for patchs in all_patches:
        for patch in patchs:
            if res == "SUCCESS":
                build_status = 1
            elif res == "UNSTABLE":
                build_status = -1
            else:
                build_status = 0

            gerrit_verify(
                conf_file=gerrit_conf_file,
                patch=patch,
                status=build_status,
                msg=job_url,
            )


@click.command()
@click.option("--pytest-params", help="Pytest params to pass to the job")
@click.option("--job", help="Job to run", required=True)
@click.option("--slave", help="Slave name to run on", required=True)
@click.option("--refs", help="Gerrit refs to run with")
@click.option("--cluster-domain", help="Cluster domain name")
@click.option("--cluster-name", help="Cluster name")
@click.option("--gerrit-conf-file", help="Gerrit config file", default=GERRIT_CONF_DEFAULT)
@click.option("--jenkins-conf-file", help="Jenkins config file", default=JENKINS_CONF_DEFAULT)
@click.option("--view", help="View already running job (send build number)", type=int)
@click.option("--progress", help="Log progress to screen", is_flag=True, default=True)
@click.option(
    "--bare-metal", help="Run on bare metal cluster", is_flag=True, default=False
)
@click.option(
    "--region",
    type=click.Choice(["USA", "EMEA"]),
    help="Region where the cluster locate",
    default="USA",
)
@click.option(
    "--report-portal",
    help="Upload results to report portal",
    is_flag=True,
    default=False,
)
def main(
    gerrit_conf_file,
    jenkins_conf_file,
    job,
    slave,
    pytest_params,
    refs,
    cluster_domain,
    cluster_name,
    view,
    progress,
    bare_metal,
    region,
    report_portal,
):
    build_job_and_verify(
        gerrit_conf_file=gerrit_conf_file,
        jenkins_conf_file=jenkins_conf_file,
        job=job,
        slave=slave,
        pytest_params=pytest_params,
        refs=refs,
        cluster_domain=cluster_domain,
        cluster_name=cluster_name,
        view=view,
        progress=progress,
        bare_metal=bare_metal,
        region=region,
        report_portal=report_portal,
    )


if __name__ == "__main__":
    main()
