# README #
Run Jenkins jobs with parameters and set Gerrit review on patches (+1, 0, -1)
based on the run results

### Installation
sudo pip install . -U
For auto-completion source auto-completion.sh in your shell rc file
    For ZSH:
        autoload -U +X compinit && compinit
        autoload -U +X bashcompinit && bashcompinit
        source <path to repo>/ArtRunner/auto-completion.sh

### Example
#### Version < 4.1
jenkins-gerrit-verify --job 4.0v4-GE-network --tag network,2 --ge_name network_dev_2 --test networking/multi_host --art refs/changes/06/83206/2

#### Version >= 4.1
######For flow job pipelines jobs can be triggered by --action
######This will trigger CLEAN_GE and INSTALL_RHV jobs:
jenkins-gerrit-verify --job rhv-master-ge-flow-network --tag network,2 --ge_name network-ge-3 --test networking/import_export --art refs/changes/28/90428/1 --action CLEAN_GE --action INSTALL_RHV

######This will trigger RUN_GE by default:
jenkins-gerrit-verify --job rhv-master-ge-flow-network --tag network,2 --ge_name network-ge-3 --test networking/import_export --art refs/changes/28/90428/1

######The view running job:
jenkins-gerrit-verify --job rhv-4.1v4-ge-runner-network --view 482
