#! /usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

SCRIPT_NAME = "jenkins-gerrit-verify"
SCRIPT = (
    "_JENKINS_GERRIT_VERIFY_COMPLETE=source {script_name} > "
    "auto-completion.sh".format(script_name=SCRIPT_NAME)
)

setup(
    name="jenkins-gerrit-verify",
    version="1.0",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Click",
        "python-jenkins",
        "colorlog",
        "python-gerrit",
        "progressbar2",
    ],
    entry_points="""
        [console_scripts]
        {script_name}=jenkins_gerrit_verify_cli.jenkins_gerrit_verify:main
    """.format(
        script_name=SCRIPT_NAME
    ),
)
