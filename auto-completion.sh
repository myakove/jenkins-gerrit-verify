_jenkins_gerrit_verify_completion() {
    COMPREPLY=( $( env COMP_WORDS="${COMP_WORDS[*]}" \
                   COMP_CWORD=$COMP_CWORD \
                   _JENKINS_GERRIT_VERIFY_COMPLETE=complete $1 ) )
    return 0
}

complete -F _jenkins_gerrit_verify_completion -o default jenkins-gerrit-verify;
